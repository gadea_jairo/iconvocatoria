﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inicio.entities
{
    class Extinguidores
    {
        private int id;
        private CATEGORIA categoria;
        private TIPO tipo;
        private MARCA marca;
        private int capacidad;
        private string unimed;
        private string lugar;
        private string fecrec;

        public Extinguidores(int id, CATEGORIA categoria, TIPO tipo, MARCA marca, int capacidad, string unimed, string lugar, string fecrec)
        {
            this.id = id;
            this.categoria = categoria;
            this.tipo = tipo;
            this.marca = marca;
            this.capacidad = capacidad;
            this.unimed = unimed;
            this.lugar = lugar;
            this.fecrec = fecrec;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        internal CATEGORIA Categoria
        {
            get
            {
                return categoria;
            }

            set
            {
                categoria = value;
            }
        }

        internal TIPO Tipo
        {
            get
            {
                return tipo;
            }

            set
            {
                tipo = value;
            }
        }

        internal MARCA Marca
        {
            get
            {
                return marca;
            }

            set
            {
                marca = value;
            }
        }

        public int Capacidad
        {
            get
            {
                return capacidad;
            }

            set
            {
                capacidad = value;
            }
        }

        public string Unimed
        {
            get
            {
                return unimed;
            }

            set
            {
                unimed = value;
            }
        }

        public string Lugar
        {
            get
            {
                return lugar;
            }

            set
            {
                lugar = value;
            }
        }

        public string Fecrec
        {
            get
            {
                return fecrec;
            }

            set
            {
                fecrec = value;
            }
        }

        public enum CATEGORIA
        {
            Americano, Europeo
        }

        public enum TIPO
        {
            Agua, Espuma, Polvo, CO2
        }

        public enum MARCA
        {
            Tornado, Amerex, Otra
        }
    }
}
