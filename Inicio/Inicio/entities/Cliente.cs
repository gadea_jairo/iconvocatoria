﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inicio.entities
{
    class Cliente
    {
        private int id;
        private string cédula;
        private string nombres;
        private string apellidos;
        private string celular;
        private string correo;
        private string dirección;
        private string municipio;
        private string departamento;

        public Cliente(int id, string cédula, string nombres, string apellidos, string celular, string correo, string dirección, string municipio, string departamento)
        {
            this.id = id;
            this.cédula = cédula;
            this.nombres = nombres;
            this.apellidos = apellidos;
            this.celular = celular;
            this.correo = correo;
            this.dirección = dirección;
            this.municipio = municipio;
            this.departamento = departamento;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Cédula
        {
            get
            {
                return cédula;
            }

            set
            {
                cédula = value;
            }
        }

        public string Nombres
        {
            get
            {
                return nombres;
            }

            set
            {
                nombres = value;
            }
        }

        public string Apellidos
        {
            get
            {
                return apellidos;
            }

            set
            {
                apellidos = value;
            }
        }

        public string Celular
        {
            get
            {
                return celular;
            }

            set
            {
                celular = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public string Direccion
        {
            get
            {
                return dirección;
            }

            set
            {
                dirección = value;
            }
        }

        public string Municipio
        {
            get
            {
                return municipio;
            }

            set
            {
                municipio = value;
            }
        }

        public string Departamento
        {
            get
            {
                return departamento;
            }

            set
            {
                departamento = value;
            }
        }
    }
}
