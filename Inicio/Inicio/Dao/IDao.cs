﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inicio.Dao
{
        interface IDao<T>
        {
            void save(T t);
            int update(T t);
            bool delete(T t);
            List<T> findAll();

    }
}
